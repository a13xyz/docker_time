
FROM debian:9
RUN apt-get update && apt-get install -y wget gcc make
RUN apt-get install wget
RUN apt-get install libpcre3-dev -y
RUN apt-get install --reinstall zlibc zlib1g zlib1g-dev -y
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install
COPY . ./app
CMD ["bash", "./app/timet.sh"]



#CMD ["bash", "timet.sh"]